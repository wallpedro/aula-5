<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Estoque extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('EstoqueModel', 'stok');
    }

    public function index(){
        $this->load->view('common/header');
        $this->load->view('intro');
        $this->load->view('common/footer');
    }

    public function lista(){
        $this->load->view('common/header');

        $data['table'] = $this->stok->lista();
        $this->load->view('lista_setor', $data);
        $this->load->view('common/footer');
    }

    public function produto(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->view('produto/ofertas');
        $this->load->view('common/rodape');
        $this->load->view('common/footer');
    }

    public function setor($id){
        $this->load->view('common/header');
        $this->load->view('common/navbar');

        $data = $this->stok->get_data($id);
        $v['titulo1'] = $this->load->view('produto/label_setor', $data, true);
        $v['setor1'] = $this->load->view('produto/setor1', $data, true);
        $v['figure'] = $this->load->view('produto/figure', $data, true);
        $v['form1'] = $this->load->view('produto/form1', '', true);
        $this->load->view('produto/layout_setor', $v);
        $this->load->view('common/rodape');
        $this->load->view('common/footer');
    }

    public function cria_setor(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->stok->novo_setor();
        $this->load->view('produto/form_cria_setor');
        $this->load->view('common/rodape');
        $this->load->view('common/footer');
    }

    public function editar($id){
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        
        // realiza a atualização no bd
        $this->stok->editar_setor($id);

        // carrega os dados do registro a ser editado
        $data = $this->stok->get_data($id);        
        $this->load->view('produto/form_cria_setor', $data);
        $this->load->view('common/rodape');
        $this->load->view('common/footer');
    }

    public function deletar($id){
        $this->stok->deletar($id);
        redirect('estoque/lista');
    }
}