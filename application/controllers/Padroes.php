<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Padroes extends CI_Controller {

    public function strategy(){
        $this->load->model('PadroesModel', 'padroes');
        $this->padroes->strategy();
    }

    public function listener(){
        $this->load->model('PadroesModel', 'padroes');
        $this->padroes->listener();
    }
}