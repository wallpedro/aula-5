<?php 

defined('BASEPATH') OR exit('No direct script access allowed');


class Component extends CI_Controller{

    public function panel(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');

        $this->load->model('ComponentModel', 'model');
        $data['component'] = $this->model->panel_sample();
        $this->load->view('component/component_view', $data);

        $this->load->view('common/rodape');
        $this->load->view('common/footer');
    }

    public function table(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');

        $this->load->model('ComponentModel', 'model');
        $data['component'] = $this->model->table_sample();
        $this->load->view('component/component_view', $data);

        $this->load->view('common/rodape');
        $this->load->view('common/footer');
    }

}