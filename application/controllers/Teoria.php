<?php 
    defined('BASEPATH') or exit('No direct script access allowed');

    class Teoria extends CI_Controller{
        
        public function heranca(){
            $this->load->model('TeoriaModel', 'teoria'); //'teoria' é um apelido para o model
            $this->teoria->heranca(); //model sendo utilizado a partir de seu apelido
        }

        public function polimorfismo(){
            $this->load->model('TeoriaModel', 'teoria');
            $this->teoria->polimorfismo();
        }
    }
