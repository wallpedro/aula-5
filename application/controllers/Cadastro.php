<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cadastro extends CI_Controller{
    public function index(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');

        $this->load->model('Cadastro/CadastroModel', 'cadastro');
        $this->cadastro->salvar();

        $this->load->view('Cadastro/form_dados_pessoais');
        
        $this->load->view('Cadastro/form_social');
        $this->load->view('Cadastro/form_endereco');
        
        

        $this->load->view('common/rodape');
        $this->load->view('common/footer');
    }
}