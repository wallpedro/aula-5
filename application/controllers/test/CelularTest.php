<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . '/controllers/test/MyToast.php');
include APPPATH.'/libraries/Test/Celular.php';

class CelularTest extends MyToast
{

    function __construct()
    {
        parent::__construct('HelloTest');
    }

    function test_celular_tem_marca(){
        $celular = new Celular('BlackBerry');
        $this->_assert_equals('BlackBerry', $celular->getMarca(), 'Marca do celular não confere');
    }

    function test_preco_celular_is_dinamico(){
        $cel = new Celular('');
        $p0 = $cel->getPreco();
        $this->_assert_equals_strict(0, $p0, 'O preco inicial deveria ser 0, porém '.$p0.' foi encontrado');

    }
}