<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . '/controllers/test/MyToast.php');
include APPPATH.'/libraries/Test/Produto.php';

class ProdutoTest extends MyToast{

    function __construct(){
        parent::__construct('ProdutoTest');
    }

    function test_produto_tem_mais_que_dois_caracteres(){

        //cenário 1
        $p = new Produto('aa');
        $n1 = $p->getNome();

        $this->_assert_equals('aa', $n1, 'Esperado: 2, recebido:'. $n1);
        
        //Cenário 2
        $p2 = new Produto('x');
        $n2 = $p2->getNome();
        
        $this->_assert_equals(-1, $n2, 'Esperado: -1, recebido:'. $n2);
    }

    function test_produto_tem_menos_que_80_caracteres(){

        $p = new Produto('12345123451234512345123451234512345123451234512345123451234512345123451234512345');
        $n1 = $p->getNome();

        $this->_assert_equals('12345123451234512345123451234512345123451234512345123451234512345123451234512345', $n1, 'Esperado: 2, recebido:'. $n1);
        
        //Cenário 2
        $p2 = new Produto('12345123451234512345123451234512345123451234511231231231232345123451234755578555s5');
        $n2 = $p2->getNome();
        
        $this->_assert_equals(-1, $n2, 'Esperado: -1, recebido:'. $n2);
    }

    function test_preco_inicia_zero(){
        
        $p = new Produto("Produto");
        $preco1 = $p->getPreco();

        $this->_assert_equals_strict(0, $preco1, 'Esperado: -1, recebido:'. $preco1);
    }

    function test_preco_permite_alteracoes(){

        $p = new Produto("Produto");
        $preco1 = $p->getPreco();
        $p->setPreco(15);
        $preco2 = $p->getPreco();
        $this->_assert_equals(15, $preco2, 'Esperado: 15, recebido:'. $preco2);        
        $this->_assert_not_equals($preco1, $preco2, 'São iguais');
    }

    function test_quantidade_inicia_1(){
        $p = new Produto('Produto');
        $qtd1 = $p->getQuantidade();
        $this->_assert_equals(1, $qtd1, 'Esperado: 1, recebido:'. $qtd1);
    }

}