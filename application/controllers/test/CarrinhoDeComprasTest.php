<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . '/controllers/test/MyToast.php');
include APPPATH.'/libraries/Test/CarrinhoDeCompras.php';
include_once APPPATH.'/libraries/Test/Produto.php';

class CarrinhoDeComprasTest extends MyToast{

    function __construct(){
        parent::__construct('CarrinhoDeComprasTest');
    }


    public function test_carrinho_inicia_com_0_produtos(){

        $carr = new CarrinhoDeCompras();
        $qtdItens = $carr->getQtd();
        $this->_assert_equals_strict(0, $qtdItens, 'Esperado: -1, recebido:'. $qtdItens);
    }

    public function test_carrinho_insere_produto(){
        $c = new CarrinhoDeCompras();
        $produto = new Produto('Produto');
        $c->addProduto($produto);
        $qtdItens = $c->getQtd();
        $this->_assert_equals(1, $qtdItens, 'Esperado: 1, recebido:'. $qtdItens);
        $produtoFromLista = $c->getProduto(0);
        $assert = $produtoFromLista === $produto;
        $this->_assert_true($assert, 'Produtos sao diferentes');
    }

    public function test_carrinho_remove_produto(){

        $p1 = new Produto('Produto 1');
        $p2 = new Produto('Produto 2');
        $p3 = new Produto('Produto 3');
        $p4 = new Produto('Produto 4');

        $carro = new CarrinhoDeCompras();

        $carro->addProduto($p4);
        $carro->addProduto($p4);
        $carro->addProduto($p4);
        $carro->addProduto($p4);

        $qtdItens = $carro->getQtd();

        $this->_assert_equals(4, $qtdItens, 'Esperado: 4, recebido:'. $qtdItens);

        $carro->removeProduto($p4);
        $qtdItens = $carro->getQtd();

        $this->_assert_equals(3, $qtdItens, 'Esperado: 3, recebido:'. $qtdItens);





    }




}