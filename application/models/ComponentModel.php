<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'libraries/component/Panel.php';
include APPPATH.'libraries/component/PanelData.php';
include APPPATH.'libraries/Storage.php';
include APPPATH.'libraries/component/Table.php';

class ComponentModel extends CI_Model{

    public function panel_sample(){
        /*$v['title'] = 'Meu primeiro componente';
        $v['subtitle'] = 'A classe que vai economizar muito tempo';
        $v['content'] = 'Na aula de hoje, tivemos o prazer de começar 
        a estudar a criação de classes em PHP... isso nos permitirá criar 
        sistemas cada vez mais sofisticados.';
        $v['link1'] = 'Saber mais';
        $v['link2'] = 'Página principal';

        $panel = new Panel($v);
        $html = $panel->getHTML();
        $html .= $panel->getHTML();
        $html .= $panel->getHTML();
        $html .= $panel->getHTML();
        $html .= $panel->getHTML();
        $html .= $panel->getHTML();
        return $html;*/ // *****FEITO NA AULA 4****


        // $pd = new PanelData();
        // $v = $pd->load(10);
        // $html = '';

        // foreach($v as $panel_data){
        //     $panel = new Panel($panel_data);
        //     $html .= $panel->getHTML();
        // }

        // return $html; ****FEITO NA AULA 5 ****

        $html = '';
        
        for($i = 0; $i < 12; $i++){
            $pd = new PanelData($i+1);
            $panel = new Panel($pd);
            $html .= $panel->getHTML();
        }
        return $html;


    }

    public function table_sample(){
        $this->load->library('Storage');
        $data = $this->storage->lista_setor();
        $label = array('Id', 'Setor', 'Imagem', 'Título Card', 'Descrição', 'Figura', 'Titulo da Figura', 'Subtítulo', 'Criado em:',);
        $tabela = new Table($data, $label);
        $tabela->setHeaderColor('secondary-color-dark');
        $tabela->useTextoBranco();
        $tabela->useZebraTable();
        $tabela->useBorder();
        $tabela->useHover();
        $tabela->useSmall();
        return $tabela->getHTML();
    }

}