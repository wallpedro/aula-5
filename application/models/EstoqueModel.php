<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'libraries/Storage.php';

class EstoqueModel extends CI_Model{

    // atributo de classe
    private $storage;

    function __construct(){
        $this->storage = new Storage();
    }

    public function get_data($id){
        return $this->storage->carregar($id);
    }

    public function novo_setor(){
        if(sizeof($_POST) == 0) return;
        $data = $this->input->post();
        return $this->storage->criar($data);
    }

    public function editar_setor($id){
        if(sizeof($_POST) == 0) return;

        $data = $this->input->post();
        $this->storage->editar($data, $id);
    }

    public function lista(){
        $v = $this->storage->lista_setor();
        $html = '';

        foreach ($v as $setor) {
            $html .= '<tr>';
            $html .= '<td>'.$setor['title'].'</td>';
            $html .= '<td>'.$setor['image'].'</td>';
            $html .= '<td>'.$setor['label'].'</td>';
            $html .= '<td>'.$setor['card_title'].'</td>';
            $html .= '<td>'.$this->get_action_buttons($setor['id']).'</td>';
            $html .= '</tr>';
        }
        return $html;
    }

    private function get_action_buttons($id){
        $html = '<a href="'.base_url('estoque/editar/'.$id).'"><i class="fas fa-edit mr-3 text-primary"></i></a>';
        $html .= '<a href="'.base_url('estoque/deletar/'.$id).'"><i class="far fa-trash-alt text-danger"></i></a>';
        return $html;
    }

    public function deletar($id){
        $this->storage->deletar($id);
    }
}

/*
 * $data['title'] = 'Produtos Químicos';
    $data['image'] = 40;
    $data['label'] = 'Saber mais...';
    $data['card_title'] = 'Aluno em laboratório - IFSP';
    $data['descr'] = 'Nossos alunos aprendem na prática em laboratórios de última geração';
    $data['figure'] = 70;
    $data['fig_title'] = 'Uma noite estrelada';
 */