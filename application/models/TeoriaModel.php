<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/teoria/Pessoa.php';
include_once APPPATH.'libraries/teoria/Aluno.php';
include_once APPPATH.'libraries/teoria/Professor.php';
include_once APPPATH.'libraries/teoria/Funcionario.php';
include_once APPPATH.'libraries/teoria/Biblioteca.php';


class TeoriaModel extends CI_Model{
    
    public function heranca(){


        $aluno1 = new Aluno('Igor', 18, 'B');
        $aluno2 = new Aluno('Jean', 22, 'B');

        var_dump($aluno1);
        echo "<br/>";
        var_dump($aluno2);

        echo "<br/><hr/><br/>";
        echo "*Nome: ".$aluno1->getNome()." *Idade: ".$aluno1->getIdade()." *Pressao: ".$aluno1->getPressao()." *Posição: ".$aluno1->getPressao()." *Turma: ".$aluno1->getTurma();
        echo "<br/><br/>";
        echo "*Nome: ".$aluno2->getNome()." *Idade: ".$aluno2->getIdade()." *Pressao: ".$aluno2->getPressao()." *Posição: ".$aluno2->getPressao()." *Turma: ".$aluno2->getTurma();

    }

    public function polimorfismo(){
        $prof = new Professor('Pardal', 'IFSP', 50);
        $func = new Funcionario('Funcionario', 'Porteiro', 50);
        $aluno = new Aluno('Aluno', 21, 'A');



        $lista = [2, 5, 7, 3, 9, 4];

        $bib = new Biblioteca();

        echo "Empréstimo para professor: <br/>";
        $bib->emprestimo($lista, $prof);

        echo "<br><br>Empréstimo para funcionario: <br/>";
        $bib->emprestimo($lista, $func);

        echo "<br><br>Empréstimo para aluno: <br/>";
        $bib->emprestimo($lista, $aluno);

    }







}