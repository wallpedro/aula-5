<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'libraries/Cadastro/dadosPessoaisValidator.php';
include APPPATH.'libraries/Cadastro/EnderecoValidator.php';
include APPPATH.'libraries/Cadastro/Endereco.php';
include APPPATH.'libraries/Cadastro/DadosPessoais.php';
include APPPATH.'libraries/Cadastro/Social.php';
include APPPATH.'libraries/Cadastro/SocialValidator.php';



class CadastroModel extends CI_Model{
    public function salvar(){
        if(sizeof($_POST) == 0){
            return; 
        } 
        $dados = new dadosPessoaisValidator();
        $dados->set_rules();

        $endereco = new EnderecoValidator();
        $endereco->set_rules();

        $social = new SocialValidator();
        $social->set_rules();

        //Executa a validação dos dados inseridos
        if($this->form_validation->run()){
            
            $v = $dados->get_data();
            $dp = new DadosPessoais();
            $id_dados_pessoais = $dp->salvar($v);

            if($id_dados_pessoais > 0){

                //Inserção de todas as entidades dependentes
                $input_social = $social->get_data();
                $soc = new Social();
                $soc->salvar($input_social, $id_dados_pessoais);

                if($id_dados_pessoais > 0){
                    
                    $input_end = $endereco->get_data();
                    $end = new Endereco();
                    $id_dados_pessoais = $end->salvar($input_end, $id_dados_pessoais);
                }
            }
        }

    }
}