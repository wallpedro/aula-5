<?php

defined('BASEPATH') or exit('No direct script access allowed');

include APPPATH . '\libraries\Padroes\observer\Estoque.php';
include APPPATH . '\libraries\Padroes\observer\Loja.php';
include APPPATH . '\libraries\Padroes\observer\MailManager.php';
include APPPATH . '\libraries\Padroes\observer\SmsManager.php';


class PadroesModel extends CI_Model {

    public function strategy(){
        $this->load->library('padroes/strategy/CalculaTaxa.php', null , 'taxa');
        $valor = 58.4;
        echo "Valor: $valor - Taxa PayPal: " . $this->taxa->calcula($valor, 'PayPal');
        echo "<br>Valor: $valor - Taxa PagSeguro: " . $this->taxa->calcula($valor, 'PagSeguro');
        echo "<br>Valor: $valor - Taxa PagarMe: " . $this->taxa->calcula($valor, 'PagarMe');
    }

    public function listener(){

        $listaUsuarios = ['Maria', 'João', 'Paulo'];

        $estoque = new Estoque('2019-05-13');
        $smsManager = new SmsManager($listaUsuarios);
        $mailManager = new MailManager($listaUsuarios);
        $loja = new Loja();

        $loja->addObserver($estoque);
        $loja->addObserver($mailManager);
        $loja->addObserver($smsManager);

        $loja->vendaRealizada();


        echo '<br><br>Removido o listener de estoque<br><br>';

        //Còdigo que condiciona a remoção do listener ( a ser implementado )
        $loja->removeObserver($estoque);



        $loja->vendaRealizada();



    }
}