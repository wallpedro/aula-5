<?php


class Celular
{

    private $marca;
    private $preco;

    function __construct($marca)
    {
        $this->marca = $marca;
        $this->preco = 0;
    }


    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * @return int
     */
    public function getPreco()
    {
        return $this->preco;
    }

}