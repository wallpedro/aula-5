<?php

include_once 'Produto.php';

class CarrinhoDeCompras{

    private $lista;

    function __construct(){
        $this->lista = [];
    }

    public function getQtd(){
        return count($this->lista);
    }

    public function getProduto($index){
        return $this->lista[$index];
    }

    public function addProduto(Produto $produto){
        $this->lista[] = $produto;
    }

    public function removeProduto(Produto $produto){
        
        foreach ($this->lista as $key => $itemLista){
            if($produto === $itemLista){
                unset($this->lista[$key]);
            }
        }
    }

}