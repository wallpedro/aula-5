<?php

class Produto {

    private $nome;
    private $preco;
    private $quantidade;

    function __construct($nome){
        $this->nome = $nome;
        $this->preco = 0;
        $this->quantidade = 1;
    }

    public function getNome(){
        if(strlen($this->nome) > 80 || strlen($this->nome) < 2){
            return -1;
        }
        return $this->nome;
    }

    public function getPreco(){
        return $this->preco;
    }

    public function setPreco($preco){
        $this->preco = $preco;
    }

    public function getQuantidade(){
        return $this->quantidade;
    }

}