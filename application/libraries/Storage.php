<?php 

class Storage{
    private $db;

    function __construct(){
        $ci = & get_instance();
        $this->db = $ci->db;
    }

    public function editar($data, $id){
        $this->db->update('setor_estoque', $data, "id = $id");
    }

    public function carregar($id){
        $sql = "SELECT * FROM setor_estoque WHERE id = $id";
        $res = $this->db->query($sql);
        $data = $res->result_array();
        return $data[0];
    }

    public function criar($data){
        $this->db->insert('setor_estoque', $data);
    }

    public function lista_setor(){
        $rs = $this->db->get('setor_estoque');
        return $rs->result_array();
    }

    public function deletar($id){
        $this->db->delete('setor_estoque', "id = $id");
    }
}