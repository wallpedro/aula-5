<?php

class PanelData{
    
    private $db;
    private $data;
    private $color;
    
    function __construct($id = null){
        $ci = & get_instance();
        $this->db = $ci->db;

        if($id) $this->start($id);
    }

    private function start($id){
        $rs = $this->db->get_where('panel_data', "id = $id");
        $v = $rs->row();

        foreach($v as $nome => $valor){
            $this->$nome = $valor;
        }
    }


    public function getColor($index){
        $v = array('primary', 'secondary', 'danger', 'alert', 'warning', 'success', 'info', 'light');
        return $v[$index % 8];
    }


    /*
    * Retorna $num registros da tabela panel_data;
    * Caso $num seja nulo, retorna todos os registros.
    * @param int | null num : a quantidade de registros informados.
    * @return array (object);
    */
    public function load($num = null){
        $n = $num ? $num : PHP_INT_MAX;
        $result_set = $this->db->get('panel_data', $n);
        $this->data = $result_set->result();
        return $this->data;
    }

}




?>