<?php

class Table{
    private $data;
    private $labels;
    private $header_bg;
    private $texto_branco;
    private $zebra_table;
    private $class_list = array('table');


    /**
     * inicializa os atributos do objeto
     * @param array data: matriz de dados da tabela
     * @param array label: lista de rótulos das colunas da tabela
     */
    public function __construct($data, $labels){
        $this->data = $data;
        $this->labels = $labels;
    }


    private function getClasses(){
        return implode(' ', $this->class_list);
    }

    //gera o cabeçalho da tabela
    private function header(){
        $html = '<thead class="'.$this->header_bg.' '.$this->zebra_table.' '.$this->texto_branco.'"><tr>';

        foreach($this->labels as $label){
            $html .= '<th scope="col">'.$label.'</th>';
        }
        $html .= '</tr></thead>';

        return $html;
    }

    private function body(){
        $html = '<tbody>';

        foreach($this->data as $row){
            $html .= '<tr>';
            
            foreach($row as $col){
                $html .= '<td>'.$col.'</td>';
            }
            
            $html .= '</tr>';
        }
        $html .= '</tbody>';

        return $html;
    }



    /**
     * o que faz? Gera o código HTML da tabela
     * o que recebe? n/a
     * o que retorna? @return string: código html da tabela 
     */

    public function getHTML(){
        $html = '<table class="'.$this->getClasses().'">';

        $html .= $this->header();
        $html .= $this->body();

        $html .= '</table>';
        return $html;
    }

    public function setHeaderColor($color){
        $this->header_bg = $color;
    }

    public function useTextoBranco(){
        $this->texto_branco = 'text-white';
    }

    public function useZebraTable(){
        $this->zebra_table = 'table-striped';
    }
    public function useBorder(){
        $this->class_list[] = 'table-bordered';
    }
    public function useHover(){
        $this->class_list[] = 'table-hover';
    }
    public function useSmall(){
        $this->class_list[] = 'table-sm';
    }

    
}