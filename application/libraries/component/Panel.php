<?php 

class Panel{

    // atributos da classe
    private $title;
    private $subtitle;
    private $content;
    private $link1;
    private $link2;
    private $color;

    // function __construct(array $data){
    //     $this->title = $data['title'];
    //     $this->subtitle = $data['subtitle'];
    //     $this->content = $data['content'];
    //     $this->link1 = $data['link1'];
    //     $this->link2 = $data['link2'];
    // }

    function __construct($data){
        $this->title = $data->title;
        $this->subtitle = $data->subtitle;
        $this->content = $data->content;
        $this->link1 = $data->link1;
        $this->link2 = $data->link2;
        $this->color = $data->getColor(rand(0,7));
    }

    public function getHTML(){
        $html = '
        <div class="card col-md-3 mt-3 ml-3 bg-'.$this->color.'">'
        .$this->card_body().
        '</div>';
        return $html;
    }

    private function card_body(){
      $html = '
      <div class="card-body">'
      .$this->card_content().   
      '</div>';
      return $html;
    }

    private function card_content(){
      $html = '
          <h5 class="card-title">'.$this->title.'</h5>
          <h6 class="card-subtitle mb-2 text-muted">'.$this->subtitle.'</h6>
          <p class="card-text">'.$this->content.'.</p>
          <a href="#!" class="card-link">'.$this->link1.'</a>
          <a href="#!" class="card-link">'.$this->link2.'</a>';
          return $html;
    }

}