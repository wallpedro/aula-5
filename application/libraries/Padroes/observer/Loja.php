<?php

include_once 'PagamentoObserver.php';

class Loja
{

    private $observerList = array();

    public function addObserver(PagamentoObserver $observer){
        $this->observerList[] = $observer;
    }

    public function removeObserver(PagamentoObserver $observer){
        foreach ($this->observerList as $key => $value){
            if($observer == $value){
                unset($this->observerList[$key]);
            }
        }
    }

    private function notify($dadosPagamento){
        foreach($this->observerList as $observer){
            $observer->pagamentoRecebido($dadosPagamento);
        }
    }

    public function vendaRealizada(){
        $dadosPagamento = new StdClass();
        $dadosPagamento->valor = 15.5;
        $dadosPagamento->produto = 'Sapato';
        $dadosPagamento->usuario_id = 1;
        $this->notify($dadosPagamento);
    }
}