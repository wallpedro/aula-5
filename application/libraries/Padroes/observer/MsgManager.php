<?php


abstract class MsgManager
{
    protected $listaDeUsuarios;

    function __construct($listaDeUsuarios)
    {
        $this->listaDeUsuarios = $listaDeUsuarios;
    }

    public function pagamentoRecebido($dadosPagamento)
    {
        $usuario_id = $dadosPagamento->usuario_id;
        $this->enviaConfirmacao($usuario_id);
    }


    protected function enviaConfirmacao($usuario_id){
        $usuario = $this->listaDeUsuarios[$usuario_id];
        $this->enviaMsgConfirmacao($usuario);
    }

    abstract protected function enviaMsgConfirmacao($usuario);
}