<?php


interface PagamentoObserver
{
    public function pagamentoRecebido($dadosPagamento);

    public function pagamentoCancelado($dadosPagamento);
}