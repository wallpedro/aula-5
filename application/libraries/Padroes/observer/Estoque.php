<?php

include_once 'PagamentoObserver.php';

class Estoque implements PagamentoObserver
{
    private $data;

    public function __construct($data) {
        $this->data = $data;
    }

    private function reposicao($produto) {
        echo '<br>Solicitar reposição do produto: ' . $produto;
    }

    public function pagamentoRecebido($dadosPagamento) {
        $produto = $dadosPagamento->produto;
        $this->reposicao($produto);
    }

    public function pagamentoCancelado($dadosPagamento){}
}