<?php

include_once 'Gateway.php';

class PagSeguro implements Gateway {

    public function calcula($valor){
        if($valor > 250){
            return $valor * 0.03;
        }
        return $valor * 0.032;
    }

}