<?php

include_once 'Gateway.php';

class PayPal implements Gateway {

    public function calcula($valor){
        if($valor > 250){
            return $valor * 0.042;
        }
        return $valor * 0.045;
    }
}
