<?php

include_once 'PayPal.php';
include_once 'PagarMe.php';
include_once 'PagSeguro.php';


class CalculaTaxa {

    public function calcula($valor, $gateway){

        $pagamento = new $gateway;
        return $pagamento->calcula($valor);

    }

}