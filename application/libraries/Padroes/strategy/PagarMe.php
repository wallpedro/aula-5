<?php

include_once 'Gateway.php';

class PagarMe implements Gateway {

    public function calcula($valor){
        if($valor > 250){
            return $valor * 0.015;
        }
        return $valor * 0.019;
    }
}