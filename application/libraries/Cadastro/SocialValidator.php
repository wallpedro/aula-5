<?php

class SocialValidator extends CI_Object{

    /*private $form_validation;

    function __construct(){
        $ci = & get_instance();
        $this->form_validation = $ci->form_validation;
    }
    */

    public function set_rules(){

        $this->form_validation->set_rules('facebook','facebook','required');
        $this->form_validation->set_rules('twitter','twitter','required');
        $this->form_validation->set_rules('instagram','instagram','required');
        $this->form_validation->set_rules('linkedin','linkedin','required');
    }

    public function get_data(){
        $data['facebook'] = $this->input->post('facebook');
        $data['twitter'] = $this->input->post('twitter');
        $data['instagram'] = $this->input->post('instagram');
        $data['linkedin'] = $this->input->post('linkedin');
        return $data;
    }


}