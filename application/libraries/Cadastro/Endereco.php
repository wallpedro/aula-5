<?php

include_once 'DAO.php';

class Endereco extends DAO{

    function __construct(){
        $this->table = 'endereco';
    }

    //sobrescrita de método
    public function salvar($dados, $id_dados_pessoais = 0){
        $dados['id_dados_pessoais'] = $id_dados_pessoais;
        parent::salvar($dados);
    }
}