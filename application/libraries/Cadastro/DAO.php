<?php
 
 class DAO extends CI_Object{
    //atributos protegidos sao acessíveis nas classes filhas
    protected $table;

    public function salvar($dados){

        $this->db->insert($this->table, $dados);
        $id = $this->db->insert_id();
        return $id > 0 ? $id : -1;
    }
 }