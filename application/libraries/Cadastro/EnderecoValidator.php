<?php

class EnderecoValidator extends CI_Object{

    /*private $form_validation;

    function __construct(){
        $ci = & get_instance();
        $this->form_validation = $ci->form_validation;
    }
    */

    public function set_rules(){

        $this->form_validation->set_rules('tipo_logradouro','tipo_logradouro','required|max_length[20]');
        $this->form_validation->set_rules('nome_logradouro','nome_logradouro','required|max_length[100]');
        $this->form_validation->set_rules('numero','numero','required');
        $this->form_validation->set_rules('complemento','complemento','required');
        $this->form_validation->set_rules('cep','cep','required');
        $this->form_validation->set_rules('bairro','bairro','required');
        $this->form_validation->set_rules('cidade','cidade','required');
        $this->form_validation->set_rules('estado','estado','required');
    }

    public function get_data(){
        $data['tipo_logradouro'] = $this->input->post('tipo_logradouro');
        $data['nome_logradouro'] = $this->input->post('nome_logradouro');
        $data['numero'] = $this->input->post('numero');
        $data['complemento'] = $this->input->post('complemento');
        $data['cep'] = $this->input->post('cep');
        $data['bairro'] = $this->input->post('bairro');
        $data['cidade'] = $this->input->post('cidade');
        $data['estado'] = $this->input->post('estado');

        return $data;
    }


}