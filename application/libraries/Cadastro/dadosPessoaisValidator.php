<?php

class dadosPessoaisValidator extends CI_Object{

    /*private $form_validation;

    function __construct(){
        $ci = & get_instance();
        $this->form_validation = $ci->form_validation;
    }
    */

    public function set_rules(){

        $this->form_validation->set_rules('nome','nome','required|max_length[20]|min_length[2]');
        $this->form_validation->set_rules('sobrenome','sobrenome','max_length[100]');
        $this->form_validation->set_rules('nascimento','nascimento','required|max_length[10]|min_length[6]');
        $this->form_validation->set_rules('rg','rg','required|exact_length[13]');
        $this->form_validation->set_rules('cpf','cpf','required|exact_length[14]');
    }

    public function get_data(){
        $data['nome'] = $this->input->post('nome');
        $data['sobrenome'] = $this->input->post('sobrenome');
        $data['nascimento'] = $this->input->post('nascimento');
        $data['rg'] = $this->input->post('rg');
        $data['cpf'] = $this->input->post('cpf');

        return $data;
    }


}