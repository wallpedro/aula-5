<?php

include_once 'DAO.php';

class Social extends DAO{

    function __construct(){
        $this->table = 'social';
    }

    //sobrescrita de método
    public function salvar($dados, $id_dados_pessoais = 0){
        $dados['id_dados_pessoais'] = $id_dados_pessoais;
        parent::salvar($dados);
    }
}