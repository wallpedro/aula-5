<?php
include_once 'Pessoa.php';

class Aluno extends Pessoa
{
    /**
     * Extends inclui os atributos da classe Pessoa
     */

    private $turma;

    function __construct($nome, $idade = 0, $turma)
    {
        parent::__construct($nome, $idade);
        $this->turma = $turma;
    }

    public function limiteLivros()
    {
        return 7;
    }

    public function limiteTempo()
    {
        return 21;
    }


    public function getTurma()
    {
        return $this->turma;
    }

    public function setTurma($turma)
    {
        $this->turma = $turma;
    }


}