<?php

include_once 'Pessoa.php';

class Professor extends Pessoa{
    //atributos

    private $especialidade;
    private $escola;

    function __construct($nome, $escola, $idade = 0){

        parent::__construct($nome, $idade);
        $this->escola = $escola;
    }

    public function limiteLivros(){
        return 10;
    }

    public function limiteTempo()
    {
        return 30;
    }


    public function getEscola() {
        return $this->escola;
    }




}