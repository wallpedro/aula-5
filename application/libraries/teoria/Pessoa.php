<?php

abstract class Pessoa {
    //atributos
    private $nome;
    private $idade;
    private $pressao;

    /**
     * Inicializa os atributos da pessoa.
     * @param string: nome da pessoa,
     * @param int: idade da pessoa.
     */
    function __construct($nome, $idade = 0){
        $this->nome = $nome;
        $this->idade = $idade;
    }

    //métodos acessores (getters/setters) *sempre públicos*

    /**
     * Informa o nome da pessoa
     * @return string: nome da pessoa
     */
    public function getNome(){
        return $this->nome;
    }

    /**
     * Informa a idade da pessoa
     * @return int: idade da pessoa
     */
    public function getIdade(){
        return $this->idade;
    }

    /**
     * Informa a pressao sanguinea da pessoa
     * @return string: pressao sanguinea da pessoa
     */
    public function getPressao(){
        $s = rand(3,30);
        $d = rand(3,18);
        return "$s x $d";
    }

    /**
     * Altera o nome da pessoa
     * @param string: novo nome
     */
    public function setNome($nome){
        $this->nome = $nome;
    }

    /**
     * Altera a idade da pessoa
     * @param int: nova idade da pessoa
     */
    public function setIdade($idade){
        $this->idade = $idade;
    }

    /**
     * Altera a pressao sanguinea da pessoa
     * @param int: nova pressao sanguinea
     */
    public function setPressao($pressao){
        $this->pressao = $pressao;
    }

    public function andar(){
        $x = rand(-30,30);
        $y = rand(-30,30);
        return "($x, $y)";
    }

    public abstract function limiteLivros();

    public abstract function limiteTempo();

}