<?php

include_once 'Pessoa.php';

class Funcionario extends Pessoa {

    private $funcao;

    function __construct($nome, $funcao, $idade = 0){
        parent::__construct($nome, $idade);
        $this->funcao = $funcao;
    }

    public function limiteLivros() {
        return $limiteLivros = 5;
    }

    public function getFuncao(){
        return $this->funcao;
    }

    public function limiteTempo()
    {
        return 14;
    }

}