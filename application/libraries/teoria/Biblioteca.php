<?php


class Biblioteca
{

    //"Array", neste caso, é um type hint
    public function emprestimo(Array $livros, Pessoa $usuario) {


        if(count($livros) <= $usuario->limiteLivros()) {
            //aceita e gera o ticket e a mensagem de sucesso
            echo "Empréstimo realizado! Você tem " . $usuario->limiteTempo() . " dias até a data de devolução";

        }else {
            //nega o empréstimo e gera a mensagem de erro

            echo "Falha na execução do empréstimo";
        }

    }


}