<div class="container">
    <div class="row">
        <div class="col-md-10 mx-auto">
            <table class="table">
                <thead>
                    <th>Setor</th>
                    <th>Imagem</th>
                    <th>Botão</th>
                    <th>Título</th>
                    <th></th>
                </thead>
                <tbody>
                    <?= $table ?>
                </tbody>
            </table>
        </div>
    </div>
</div>