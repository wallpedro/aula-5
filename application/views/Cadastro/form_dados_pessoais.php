<div class="container mt-5">
<?= validation_errors('<div class="alert alert-danger">', '</div>')?>
    <div class="card">
        <div class="card-header">
            <h1>Dados Pessoais</h1>
        </div>
        <div class="card-body">
            <form method="POST">
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="md-form form-group">
                            <input type="text" class="form-control" id="nome" name="nome" value="<?= set_value('nome') ?>"
                            placeholder="Nome">
                            <label for="nome">Nome</label>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="md-form form-group">
                            <input type="text" class="form-control" id="sobrenome" name="sobrenome" value="<?= set_value('sobrenome') ?>" 
                            placeholder="Sobrenome">
                            <label for="Sobrenome">Sobrenome</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="md-form form-group">
                            <input type="text" class="form-control" id="nascimento" name="nascimento" value="<?= set_value('nascimento') ?>"
                            placeholder="DD/MM/AAAA">
                            <label for="nascimento">Nascimento</label>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="md-form form-group">
                            <input type="text" class="form-control" id="rg" name="rg" value="<?= set_value('rg') ?>"
                                placeholder="RG">
                            <label for="RG">RG</label>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="md-form form-group">
                            <input type="text" class="form-control" id="cpf" name="cpf" value="<?= set_value('cpf') ?>"
                            placeholder="CPF">"
                            <label for="CPF">CPF</label>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>