<div class="container mt-5">
    <div class="card">
        <div class="card-header">
            <h1>Redes Sociais</h1>
        </div>
        <div class="card-body">
            <form method="POST">
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="md-form form-group">
                            <input type="text" class="form-control" id="facebook" name="facebook" 
                            value="<?= isset($_POST['facebook']) ? set_value('facebook') : 'facebook.com/' ?>">
                            <label for="Facebook">Facebook</label>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="md-form form-group">
                            <input type="text" class="form-control" id="twitter" name="twitter" 
                            value="<?= isset($_POST['twitter']) ? set_value('twitter') : 'twitter.com/' ?>" >
                            <label for="Twitter">Twitter</label>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="md-form form-group">
                            <input type="text" class="form-control" id="instagram" name="instagram" 
                            value="<?= isset($_POST['instagram']) ? set_value('instagram') : 'instagram.com/' ?>">
                            <label for="instagram">Instagram</label>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="md-form form-group">
                            <input type="text" class="form-control" id="linkedin" name="linkedin" 
                            value="<?= isset($_POST['linkedin']) ? set_value('linkedin') : 'linkedin.com/' ?>" >
                            <label for="LinkedIn">LinkedIn</label>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>