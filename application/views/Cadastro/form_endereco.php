<div class="container mt-5 mb-5">
    <div class="card">
        <div class="card-header">
            <h1>Endereço</h1>
        </div>
        <div class="card-body">
            <div class="form-row">
                <div class="col-md-3">
                    <div class="md-form form-group">
                        <input type="text" class="form-control" id="tipo_logradouro" name="tipo_logradouro" value="<?= set_value('tipo_logradouro') ?>" 
                            placeholder="Tipo do logradouro">
                        <label for="tipo_logradouro">Tipo do Logradouro</label>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="md-form form-group">
                        <input type="text" class="form-control" id="nome_logradouro" name="nome_logradouro" value="<?= set_value('nome_logradouro') ?>" 
                            placeholder="Nome do Logradouro">
                        <label for="nome_logradouro">Nome do Logradouro</label>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="md-form form-group">
                        <input type="number" class="form-control" id="numero" name="numero" value="<?= set_value('numero') ?>"
                         placeholder="Número">
                        <label for="numero">Número</label>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-md-3">
                    <div class="md-form form-group">
                        <input type="text" class="form-control" id="complemento" name="complemento" value="<?= set_value('complemento') ?>"
                            placeholder="Complemento">
                        <label for="complemento">Complemento</label>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="md-form form-group">
                        <input type="text" class="form-control" id="cep" name="cep" value="<?= set_value('cep') ?>" placeholder="CEP">
                        <label for="cep">CEP</label>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="md-form form-group">
                        <input type="text" class="form-control" id="bairro" name="bairro" value="<?= set_value('bairro') ?>" placeholder="Bairro">
                        <label for="bairro">Bairro</label>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-md-6">
                    <div class="md-form form-group">
                        <input type="text" class="form-control" id="cidade" name="cidade" value="<?= set_value('cidade') ?>" placeholder="Cidade">
                        <label for="cidade">Cidade</label>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="md-form form-group">
                        <input type="text" class="form-control" id="estado" name="estado" value="<?= set_value('estado') ?>" placeholder="Estado">
                        <label for="estado">Estado</label>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary btn-md">Enviar</button>
            </form>
        </div>
    </div>
</div>