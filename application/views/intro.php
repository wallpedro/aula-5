<nav class="navbar navbar-dark indigo">
  <span class="navbar-text white-text">
    Linguagem de Programação II - 2019
  </span>
</nav>

<div class="container-fluid mt-3">
    <div class="card">
        <img src="https://mdbootstrap.com/img/Others/documentation/img%20(75)-mini.jpg" alt="" class="image-fluid">
    </div>

<!-- Jumbotron -->
<div class="jumbotron text-center">

  <!-- Title -->
  <h2 class="card-title h2">Material Design for Bootstrap</h2>
  <!-- Subtitle -->
  <p class="blue-text my-4 font-weight-bold">Powerful and free Material Design UI KIT</p>

  <!-- Grid row -->
  <div class="row d-flex justify-content-center">

    <!-- Grid column -->
    <div class="col-xl-7 pb-2">

      <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga aliquid dolorem ea distinctio exercitationem delectus qui, quas eum architecto, amet quasi accusantium, fugit consequatur ducimus obcaecati numquam molestias hic itaque accusantium doloremque laudantium, totam rem aperiam.</p>

    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->

  <hr class="my-4">

  <div class="pt-2">
    <button type="button" class="btn btn-blue waves-effect">Buy now <span class="far fa-gem ml-1"></span></button>
    <button type="button" class="btn btn-outline-primary waves-effect">Download <i class="fas fa-download ml-1"></i></button>
  </div>

</div>
<!-- Jumbotron -->


</div>

