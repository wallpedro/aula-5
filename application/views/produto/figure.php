<figure class="figure">
    <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/8-col/img%20(<?= $figure ?>).jpg" class="figure-img img-fluid z-depth-1">
    <figcaption class="figure-caption"><?= $fig_title ?></figcaption>
</figure>