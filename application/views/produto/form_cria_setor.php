<div class="container">
    <div class="row">
        <div class="col-md-6 mx-auto mt-3 mb-3">
            <form method="POST" class="text-center border border-light p-5">
                <p class="h4 mb-4">Criar novo setor</p>
                <input type="text" value="<?= isset($title) ? $title : '' ?>" id="title" name="title" class="form-control mb-4" placeholder="Nome do setor">
                <input type="number" value="<?= isset($image) ? $image : '' ?>" id="image" name="image" class="form-control mb-4" placeholder="Número da Imagem">
                <input type="text" value="<?= isset($label) ? $label : '' ?>" id="label" name="label" class="form-control mb-4" placeholder="Rotulo do botão">
                <input type="text" value="<?= isset($card_title) ? $card_title : '' ?>" id="card_title" name="card_title" class="form-control mb-4" placeholder="Título do cartão">
                <div class="form-group">
                    <textarea class="form-control rounded-0" id="descr" name="descr" rows="3" placeholder="Descrição "><?= isset($descr) ? $descr : '' ?></textarea>
                </div>
                <input type="number" value="<?= isset($figure) ? $figure : '' ?>" id="figure" name="figure" class="form-control mb-4" placeholder="Figura no final da página">
                <input type="text" value="<?= isset($fig_title) ? $fig_title : '' ?>" id="fig_title" name="fig_title" class="form-control mb-4" placeholder="Título da figura">
                <button class="btn btn-info btn-block" type="submit">Enviar</button>
            </form>
        </div>
    </div>
</div>