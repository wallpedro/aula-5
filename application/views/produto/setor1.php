<div class="card">
  <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/images/<?= $image ?>.jpg" alt="Card image cap">
  <div class="card-body">
    <h4 class="card-title"><a><?= $card_title ?></a></h4>
    <p class="card-text"><?= $descr ?>.</p>
    <a href="#" class="btn btn-primary"><?= $label ?></a>
  </div>
</div>